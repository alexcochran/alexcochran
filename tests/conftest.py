"""Common resources shared between test modules."""

import pytest
from _pytest.fixtures import SubRequest


@pytest.fixture(scope="session", params=[4])
def sample_multi_line_text(request: SubRequest) -> str:
    """Return a sample multi-line string for testing formatting operations."""

    return "".join(
        f"{' ' * request.param} {line}"
        for line in r"""
  \  /       Partly cloudy
_ /"".-.     32(26) °F
  \_(   ).   ← 5 mph
  /(___(__)  9 mi
             0.0 in
    """.splitlines(keepends=True)
    )

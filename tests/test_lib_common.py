"""Tests for the common utility functions in ci/lib/common.py."""

from ci.lib import common


def test_text_left_pad(sample_multi_line_text: str) -> None:
    """Ensure text is left-padded properly according to the function inputs."""

    assert common.text_left_pad(text=sample_multi_line_text, indent_spaces=4) == sample_multi_line_text

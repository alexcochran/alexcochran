"""Environment variable modeling and handling."""

import os
from typing import Self

from loguru import logger
from pydantic import BaseModel

DEFAULT_TIMEOUT_SECS = 20


class ApiClientEnv(BaseModel):
    """Generic environment variable configuration object for API clients."""

    REQ_TIMEOUT_SECS: int = DEFAULT_TIMEOUT_SECS

    @classmethod
    def req_timeout_secs_from_env(cls) -> int:
        """Derive an API client environment from the runtime environment variables."""

        try:
            if os.environ["REQ_TIMEOUT_SECS"]:
                env_val = os.environ["REQ_TIMEOUT_SECS"]

                # Environment variables are strings, so attempt to convert to int
                try:
                    req_timeout_secs = int(env_val)
                    return req_timeout_secs
                except ValueError:
                    logger.error(f"Could not cast environment variable `REQ_TIMEOUT_SECS` to int (value = {env_val})")
                    logger.error("Returning default timeout value.")
        except KeyError:
            logger.info("No REQ_TIMEOUT_SECS value provided; using default value.")

        return DEFAULT_TIMEOUT_SECS


class WttrInClientEnv(ApiClientEnv):
    """Environment variable configuration object for the wttr.in API client."""

    WTTR_IN_BASE_URL: str
    WTTR_IN_LOCATION: str

    @classmethod
    def from_os_env(cls) -> Self:
        """Derive the environment  from the runtime environment variables."""

        return cls(
            REQ_TIMEOUT_SECS=super().req_timeout_secs_from_env(),
            WTTR_IN_BASE_URL=os.environ["WTTR_IN_BASE_URL"],
            WTTR_IN_LOCATION=os.environ["WTTR_IN_LOCATION"],
        )


class CiEnv(BaseModel):
    """Configuration object for environment variables.

    Assists in checking for missing variables during processing.
    """

    wttr_in_env: WttrInClientEnv

    @classmethod
    def from_os_env(cls) -> Self:
        """Derive the entire environment variable configuration from the runtime environment (e.g. GitLab CI/CD)."""

        return cls(wttr_in_env=WttrInClientEnv.from_os_env())

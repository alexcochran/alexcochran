"""Common helper/utility resources."""

from datetime import datetime


def readable_current_datetime() -> str:
    """Returns a human-readable datetime formatted as a string."""

    return f"{datetime.now():%Y-%m-%d %H:%M:%S%z} UTC"


def count_left_spaces(text: str) -> int:
    """Returns the spaces on the left side of a string."""

    return len(text) - len(text.lstrip())


def text_left_pad(text: str, indent_spaces: int) -> str:
    """Ensures the left-most character in a string or lines of text is exactly `offset` spaces from the left side."""

    if indent_spaces < 0:
        raise ValueError("The indent space size value cannot be less than zero.")

    left_offsets: list[int] = []
    lines = text.splitlines(keepends=True)

    # List comprehension to determine the entire collection of indent sizes
    left_offsets = [count_left_spaces(indent_spaces_size) for indent_spaces_size in lines]
    offset_diff = indent_spaces - min(left_offsets)

    if offset_diff == 0:
        return text

    # If the diff isn't 0, make the leading whitespace change and return the edited string
    return "".join([f"{' ' * offset_diff}{line}" for line in lines])

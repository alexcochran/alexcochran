"""A client for accessing simple text-based weather reports from wttr.in.

This service requires no credentials to access weather data.
"""

import urllib.parse
from typing import Self

import requests
from pydantic import BaseModel

from lib.env import WttrInClientEnv


class RequestConfig(BaseModel):
    """Query parameter object for wttr.in requests."""

    base_url: str
    location: str
    timeout_secs: int

    @property
    def full_url(self) -> str:
        """Return the full URL for the request.

        wttr.in requests use the weather report location as the second part of the URL path, {base_url}/{req_loc},
        e.g. https://wttr.in/Denver, or https://wttr.in/Denver?format=4... with query parameters.

        This property combines the base_url on the settings model with the input location.
        """

        return urllib.parse.urljoin(base=self.base_url, url=self.location)

    @classmethod
    def from_env(cls, env: WttrInClientEnv) -> Self:
        """Build a RequestConfig from an environment variable container."""

        return cls(base_url=env.WTTR_IN_BASE_URL, location=env.WTTR_IN_LOCATION, timeout_secs=env.REQ_TIMEOUT_SECS)


def get_cur_weather_report(request_config: RequestConfig) -> str | None:
    """Return a report string for the current weather."""

    try:
        res = requests.get(
            url=f"{request_config.full_url}?0T",
            timeout=request_config.timeout_secs,
        )

        if res._content:  # pylint: disable=protected-access
            return res._content.decode("utf-8")  # pylint: disable=protected-access

        return None

    except KeyError:
        # Fail gracefully
        return None

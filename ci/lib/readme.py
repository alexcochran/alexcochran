"""Module for Jinja2 templating of README files.

Template files (*.j2) should be written with the expectation that an error could occur during generation of dynamic
content (e.g. a weather report API request failure), so the template should look complete without it.
"""

from pathlib import Path

from jinja2 import Environment, FileSystemLoader, UndefinedError
from loguru import logger
from pydantic import BaseModel


class ReadmeSettings(BaseModel):
    """Settings interface for functions in the README module."""

    j2_templates_dir: Path = Path("ci/lib/jinja")
    template_filename: str = "README.md.j2"
    actual_filename: str = "README.md"

    @property
    def readme_path(self) -> Path:
        """Build the path to the README file."""

        return Path(self.actual_filename)


class DynamicContent(BaseModel):
    """Dynamic README content model for values updated by automation.

    All values should be strings for output using the Jinja template.
    """

    weather_report: str | None
    update_datetime: str


def fill_template(readme_settings: ReadmeSettings, dynamic_content: DynamicContent) -> str:
    """Apply the weather report to the README.md file in the repository."""

    j2_env = Environment(loader=FileSystemLoader(readme_settings.j2_templates_dir))
    j2_template = j2_env.get_template(name=readme_settings.template_filename)

    try:
        # Render the template with the input keyword arguments
        return j2_template.render(dynamic_content=dynamic_content)
    except UndefinedError as err:
        # Log the error, but attempt to proceed
        logger.warning("One or more dynamic content variables is undefined.")
        logger.warning(err)

    # Default render, w/o dynamic content
    logger.info("Attempting to render without dynamic content.")
    return j2_template.render()


def write_md_file(readme_path: Path, file_contents: str) -> int:
    """Write a Markdown file containing the input string to the specified path.

    Returns the number of characters written to the file as an integer.
    """

    with open(file=Path(readme_path), mode="wt", encoding="utf-8") as readme_file:
        return readme_file.write(file_contents)

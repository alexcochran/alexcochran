"""Main CI script."""

import os

from lib import env, readme
from lib.clients import wttr_in_client
from lib.common import readable_current_datetime, text_left_pad
from loguru import logger


def ci_automation() -> None:
    """CI function calls."""

    logger.info(os.environ)

    logger.info("### CI/CD SCRIPTING: START ###")
    logger.info("------------------------------")

    logger.info("Loading applicable CI environment variables...")
    ci_env = env.CiEnv.from_os_env()

    logger.info("Updating README with dynamic content...")
    readme_settings = readme.ReadmeSettings()

    wttr_in_report_response = wttr_in_client.get_cur_weather_report(
        request_config=wttr_in_client.RequestConfig.from_env(env=ci_env.wttr_in_env)
    )
    wttr_in_report_response = (
        text_left_pad(wttr_in_report_response, 4) if wttr_in_report_response else wttr_in_report_response
    )

    dynamic_readme_content = readme.DynamicContent(
        weather_report=wttr_in_report_response,
        update_datetime=readable_current_datetime(),
    )

    readme_md = readme.fill_template(readme_settings=readme_settings, dynamic_content=dynamic_readme_content)

    readme_char_len = readme.write_md_file(readme_path=readme_settings.readme_path, file_contents=readme_md)
    logger.info(f"{readme_char_len} characters written to {readme_settings.readme_path}")

    if readme_char_len == 0:
        logger.warning(f"The generated file at {readme_settings.readme_path} may be empty.")

    logger.info("### CI/CD SCRIPTING: END ###")
    logger.info("----------------------------")


if __name__ == "__main__":
    ci_automation()

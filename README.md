<!-- This README is edited via the template in ci/lib/jinja/README.md.j2. Changes to the root README.md file will be lost. -->
## Hi, I'm Alex 🏔️

I'm a software engineer living near Denver, Colorado, USA.

I keep code here and in my OSS garden at **[AlexLab Cloud][links.gitlab.alexlab-cloud]**.

---

<div align="center">

[![acochran.dev][badges.personal-website]](https://acochran.dev)
[![GitLab: alexcochran][badges.gitlab.alexlab-cloud]](https://gitlab.com/alexlab-cloud)
[![GitHub: alexcochran][badges.github.alexcochran]](https://github.com/alexcochran)

</div>
---

### My Local Weather

    Weather report: Denver
    
          \   /     Sunny
           .-.      -1(-4) °C      
        ― (   ) ―   ↑ 9 km/h       
           `-’      16 km          
          /   \     0.0 mm         

<sub>↻ Updated: 2025-01-16 19:02:46 UTC</sub>

---

<!-- Links -->
[links.gitlab.alexlab-cloud]: https://gitlab.com/alexlab-cloud

<!-- Badges -->
[badges.personal-website]: https://img.shields.io/badge/acochran.dev-blue?style=for-the-badge&color=%23470ff4
[badges.gitlab.alexlab-cloud]: https://img.shields.io/badge/GitLab%3A%20Alexlab%20Cloud-%23554488?logo=gitlab&style=for-the-badge
[badges.github.alexcochran]: https://img.shields.io/badge/GitHub%3A%20alexcochran-%23000000?logo=github&style=for-the-badge